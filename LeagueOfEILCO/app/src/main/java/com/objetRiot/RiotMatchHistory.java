package com.objetRiot;

import java.util.ArrayList;

public class RiotMatchHistory {
    ArrayList< RiotMatchResume > matches = new ArrayList < RiotMatchResume > ();
    private float startIndex;
    private float endIndex;
    private float totalGames;


    // Getter Methods

    public ArrayList<RiotMatchResume> getMatches() {
        return matches;
    }

    public float getStartIndex() {
        return startIndex;
    }

    public float getEndIndex() {
        return endIndex;
    }

    public float getTotalGames() {
        return totalGames;
    }

    // Setter Methods

    public void setStartIndex(float startIndex) {
        this.startIndex = startIndex;
    }

    public void setEndIndex(float endIndex) {
        this.endIndex = endIndex;
    }

    public void setTotalGames(float totalGames) {
        this.totalGames = totalGames;
    }

    public void setMatches(ArrayList<RiotMatchResume> matches) {
        this.matches = matches;
    }

}