package com.objetRiot;

import java.util.ArrayList;

public class RiotChampion {
    private String id;
    private String key;
    private String name;
    private String title;
    RiotImage image;
    ArrayList< RiotSkin > skins = new ArrayList < RiotSkin > ();
    private String lore;
    private String blurb;
    private String partype;
    RiotChampStat stats;
    ArrayList < RiotSpell > spells = new ArrayList < RiotSpell > ();
    RiotPassive passive;


    // Getter Methods

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public RiotImage getImage() {
        return image;
    }

    public String getLore() {
        return lore;
    }

    public String getBlurb() {
        return blurb;
    }

    public String getPartype() {
        return partype;
    }

    public RiotChampStat getStats() {
        return stats;
    }

    public RiotPassive getPassive() {
        return passive;
    }

    // Setter Methods

    public void setId(String id) {
        this.id = id;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(RiotImage imageObject) {
        this.image = imageObject;
    }

    public void setLore(String lore) {
        this.lore = lore;
    }

    public void setBlurb(String blurb) {
        this.blurb = blurb;
    }

    public void setPartype(String partype) {
        this.partype = partype;
    }

    public void setStats(RiotChampStat statsObject) {
        this.stats = statsObject;
    }

    public void setPassive(RiotPassive passiveObject) {
        this.passive = passiveObject;
    }
}
