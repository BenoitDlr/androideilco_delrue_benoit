package com.objetRiot;

public class RiotStat {
    private float participantId;
    private boolean win;
    private float item0;
    private float item1;
    private float item2;
    private float item3;
    private float item4;
    private float item5;
    private float item6;
    private float kills;
    private float deaths;
    private float assists;
    private float largestKillingSpree;
    private float largestMultiKill;
    private float killingSprees;
    private float longestTimeSpentLiving;
    private float doubleKills;
    private float tripleKills;
    private float quadraKills;
    private float pentaKills;
    private float unrealKills;
    private float totalDamageDealt;
    private float magicDamageDealt;
    private float physicalDamageDealt;
    private float trueDamageDealt;
    private float largestCriticalStrike;
    private float totalDamageDealtToChampions;
    private float magicDamageDealtToChampions;
    private float physicalDamageDealtToChampions;
    private float trueDamageDealtToChampions;
    private float totalHeal;
    private float totalUnitsHealed;
    private float damageSelfMitigated;
    private float damageDealtToObjectives;
    private float damageDealtToTurrets;
    private float visionScore;
    private float timeCCingOthers;
    private float totalDamageTaken;
    private float magicalDamageTaken;
    private float physicalDamageTaken;
    private float trueDamageTaken;
    private float goldEarned;
    private float goldSpent;
    private float turretKills;
    private float inhibitorKills;
    private float totalMinionsKilled;
    private float neutralMinionsKilled;
    private float neutralMinionsKilledTeamJungle;
    private float neutralMinionsKilledEnemyJungle;
    private float totalTimeCrowdControlDealt;
    private float champLevel;
    private float visionWardsBoughtInGame;
    private float sightWardsBoughtInGame;
    private float wardsPlaced;
    private float wardsKilled;
    private boolean firstBloodKill;
    private boolean firstBloodAssist;
    private boolean firstTowerKill;
    private boolean firstTowerAssist;
    private boolean firstInhibitorKill;
    private boolean firstInhibitorAssist;
    private float combatPlayerScore;
    private float objectivePlayerScore;
    private float totalPlayerScore;
    private float totalScoreRank;
    private float playerScore0;
    private float playerScore1;
    private float playerScore2;
    private float playerScore3;
    private float playerScore4;
    private float playerScore5;
    private float playerScore6;
    private float playerScore7;
    private float playerScore8;
    private float playerScore9;
    private float perk0;
    private float perk0Var1;
    private float perk0Var2;
    private float perk0Var3;
    private float perk1;
    private float perk1Var1;
    private float perk1Var2;
    private float perk1Var3;
    private float perk2;
    private float perk2Var1;
    private float perk2Var2;
    private float perk2Var3;
    private float perk3;
    private float perk3Var1;
    private float perk3Var2;
    private float perk3Var3;
    private float perk4;
    private float perk4Var1;
    private float perk4Var2;
    private float perk4Var3;
    private float perk5;
    private float perk5Var1;
    private float perk5Var2;
    private float perk5Var3;
    private float perkPrimaryStyle;
    private float perkSubStyle;
    private float statPerk0;
    private float statPerk1;
    private float statPerk2;


    // Getter Methods
    public String getKDA(){
        return(this.kills + "/" + this.deaths + "/" + this.assists);
    }

    public String getFullBuild()
    {
        return(this.item0 + "/" + this.item1 + "/" + this.item2 + "/" + this.item3 + "/" + this.item4 + "/" + this.item5 + "/" + this.item6);
    }
    public float getParticipantId() {
        return participantId;
    }

    public boolean getWin() {
        return win;
    }

    public float getItem0() {
        return item0;
    }

    public float getItem1() {
        return item1;
    }

    public float getItem2() {
        return item2;
    }

    public float getItem3() {
        return item3;
    }

    public float getItem4() {
        return item4;
    }

    public float getItem5() {
        return item5;
    }

    public float getItem6() {
        return item6;
    }

    public float getKills() {
        return kills;
    }

    public float getDeaths() {
        return deaths;
    }

    public float getAssists() {
        return assists;
    }

    public float getLargestKillingSpree() {
        return largestKillingSpree;
    }

    public float getLargestMultiKill() {
        return largestMultiKill;
    }

    public float getKillingSprees() {
        return killingSprees;
    }

    public float getLongestTimeSpentLiving() {
        return longestTimeSpentLiving;
    }

    public float getDoubleKills() {
        return doubleKills;
    }

    public float getTripleKills() {
        return tripleKills;
    }

    public float getQuadraKills() {
        return quadraKills;
    }

    public float getPentaKills() {
        return pentaKills;
    }

    public float getUnrealKills() {
        return unrealKills;
    }

    public float getTotalDamageDealt() {
        return totalDamageDealt;
    }

    public float getMagicDamageDealt() {
        return magicDamageDealt;
    }

    public float getPhysicalDamageDealt() {
        return physicalDamageDealt;
    }

    public float getTrueDamageDealt() {
        return trueDamageDealt;
    }

    public float getLargestCriticalStrike() {
        return largestCriticalStrike;
    }

    public float getTotalDamageDealtToChampions() {
        return totalDamageDealtToChampions;
    }

    public float getMagicDamageDealtToChampions() {
        return magicDamageDealtToChampions;
    }

    public float getPhysicalDamageDealtToChampions() {
        return physicalDamageDealtToChampions;
    }

    public float getTrueDamageDealtToChampions() {
        return trueDamageDealtToChampions;
    }

    public float getTotalHeal() {
        return totalHeal;
    }

    public float getTotalUnitsHealed() {
        return totalUnitsHealed;
    }

    public float getDamageSelfMitigated() {
        return damageSelfMitigated;
    }

    public float getDamageDealtToObjectives() {
        return damageDealtToObjectives;
    }

    public float getDamageDealtToTurrets() {
        return damageDealtToTurrets;
    }

    public float getVisionScore() {
        return visionScore;
    }

    public float getTimeCCingOthers() {
        return timeCCingOthers;
    }

    public float getTotalDamageTaken() {
        return totalDamageTaken;
    }

    public float getMagicalDamageTaken() {
        return magicalDamageTaken;
    }

    public float getPhysicalDamageTaken() {
        return physicalDamageTaken;
    }

    public float getTrueDamageTaken() {
        return trueDamageTaken;
    }

    public float getGoldEarned() {
        return goldEarned;
    }

    public float getGoldSpent() {
        return goldSpent;
    }

    public float getTurretKills() {
        return turretKills;
    }

    public float getInhibitorKills() {
        return inhibitorKills;
    }

    public float getTotalMinionsKilled() {
        return totalMinionsKilled;
    }

    public float getNeutralMinionsKilled() {
        return neutralMinionsKilled;
    }

    public float getNeutralMinionsKilledTeamJungle() {
        return neutralMinionsKilledTeamJungle;
    }

    public float getNeutralMinionsKilledEnemyJungle() {
        return neutralMinionsKilledEnemyJungle;
    }

    public float getTotalTimeCrowdControlDealt() {
        return totalTimeCrowdControlDealt;
    }

    public float getChampLevel() {
        return champLevel;
    }

    public float getVisionWardsBoughtInGame() {
        return visionWardsBoughtInGame;
    }

    public float getSightWardsBoughtInGame() {
        return sightWardsBoughtInGame;
    }

    public float getWardsPlaced() {
        return wardsPlaced;
    }

    public float getWardsKilled() {
        return wardsKilled;
    }

    public boolean getFirstBloodKill() {
        return firstBloodKill;
    }

    public boolean getFirstBloodAssist() {
        return firstBloodAssist;
    }

    public boolean getFirstTowerKill() {
        return firstTowerKill;
    }

    public boolean getFirstTowerAssist() {
        return firstTowerAssist;
    }

    public boolean getFirstInhibitorKill() {
        return firstInhibitorKill;
    }

    public boolean getFirstInhibitorAssist() {
        return firstInhibitorAssist;
    }

    public float getCombatPlayerScore() {
        return combatPlayerScore;
    }

    public float getObjectivePlayerScore() {
        return objectivePlayerScore;
    }

    public float getTotalPlayerScore() {
        return totalPlayerScore;
    }

    public float getTotalScoreRank() {
        return totalScoreRank;
    }

    public float getPlayerScore0() {
        return playerScore0;
    }

    public float getPlayerScore1() {
        return playerScore1;
    }

    public float getPlayerScore2() {
        return playerScore2;
    }

    public float getPlayerScore3() {
        return playerScore3;
    }

    public float getPlayerScore4() {
        return playerScore4;
    }

    public float getPlayerScore5() {
        return playerScore5;
    }

    public float getPlayerScore6() {
        return playerScore6;
    }

    public float getPlayerScore7() {
        return playerScore7;
    }

    public float getPlayerScore8() {
        return playerScore8;
    }

    public float getPlayerScore9() {
        return playerScore9;
    }

    public float getPerk0() {
        return perk0;
    }

    public float getPerk0Var1() {
        return perk0Var1;
    }

    public float getPerk0Var2() {
        return perk0Var2;
    }

    public float getPerk0Var3() {
        return perk0Var3;
    }

    public float getPerk1() {
        return perk1;
    }

    public float getPerk1Var1() {
        return perk1Var1;
    }

    public float getPerk1Var2() {
        return perk1Var2;
    }

    public float getPerk1Var3() {
        return perk1Var3;
    }

    public float getPerk2() {
        return perk2;
    }

    public float getPerk2Var1() {
        return perk2Var1;
    }

    public float getPerk2Var2() {
        return perk2Var2;
    }

    public float getPerk2Var3() {
        return perk2Var3;
    }

    public float getPerk3() {
        return perk3;
    }

    public float getPerk3Var1() {
        return perk3Var1;
    }

    public float getPerk3Var2() {
        return perk3Var2;
    }

    public float getPerk3Var3() {
        return perk3Var3;
    }

    public float getPerk4() {
        return perk4;
    }

    public float getPerk4Var1() {
        return perk4Var1;
    }

    public float getPerk4Var2() {
        return perk4Var2;
    }

    public float getPerk4Var3() {
        return perk4Var3;
    }

    public float getPerk5() {
        return perk5;
    }

    public float getPerk5Var1() {
        return perk5Var1;
    }

    public float getPerk5Var2() {
        return perk5Var2;
    }

    public float getPerk5Var3() {
        return perk5Var3;
    }

    public float getPerkPrimaryStyle() {
        return perkPrimaryStyle;
    }

    public float getPerkSubStyle() {
        return perkSubStyle;
    }

    public float getStatPerk0() {
        return statPerk0;
    }

    public float getStatPerk1() {
        return statPerk1;
    }

    public float getStatPerk2() {
        return statPerk2;
    }

    // Setter Methods

    public void setParticipantId(float participantId) {
        this.participantId = participantId;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public void setItem0(float item0) {
        this.item0 = item0;
    }

    public void setItem1(float item1) {
        this.item1 = item1;
    }

    public void setItem2(float item2) {
        this.item2 = item2;
    }

    public void setItem3(float item3) {
        this.item3 = item3;
    }

    public void setItem4(float item4) {
        this.item4 = item4;
    }

    public void setItem5(float item5) {
        this.item5 = item5;
    }

    public void setItem6(float item6) {
        this.item6 = item6;
    }

    public void setKills(float kills) {
        this.kills = kills;
    }

    public void setDeaths(float deaths) {
        this.deaths = deaths;
    }

    public void setAssists(float assists) {
        this.assists = assists;
    }

    public void setLargestKillingSpree(float largestKillingSpree) {
        this.largestKillingSpree = largestKillingSpree;
    }

    public void setLargestMultiKill(float largestMultiKill) {
        this.largestMultiKill = largestMultiKill;
    }

    public void setKillingSprees(float killingSprees) {
        this.killingSprees = killingSprees;
    }

    public void setLongestTimeSpentLiving(float longestTimeSpentLiving) {
        this.longestTimeSpentLiving = longestTimeSpentLiving;
    }

    public void setDoubleKills(float doubleKills) {
        this.doubleKills = doubleKills;
    }

    public void setTripleKills(float tripleKills) {
        this.tripleKills = tripleKills;
    }

    public void setQuadraKills(float quadraKills) {
        this.quadraKills = quadraKills;
    }

    public void setPentaKills(float pentaKills) {
        this.pentaKills = pentaKills;
    }

    public void setUnrealKills(float unrealKills) {
        this.unrealKills = unrealKills;
    }

    public void setTotalDamageDealt(float totalDamageDealt) {
        this.totalDamageDealt = totalDamageDealt;
    }

    public void setMagicDamageDealt(float magicDamageDealt) {
        this.magicDamageDealt = magicDamageDealt;
    }

    public void setPhysicalDamageDealt(float physicalDamageDealt) {
        this.physicalDamageDealt = physicalDamageDealt;
    }

    public void setTrueDamageDealt(float trueDamageDealt) {
        this.trueDamageDealt = trueDamageDealt;
    }

    public void setLargestCriticalStrike(float largestCriticalStrike) {
        this.largestCriticalStrike = largestCriticalStrike;
    }

    public void setTotalDamageDealtToChampions(float totalDamageDealtToChampions) {
        this.totalDamageDealtToChampions = totalDamageDealtToChampions;
    }

    public void setMagicDamageDealtToChampions(float magicDamageDealtToChampions) {
        this.magicDamageDealtToChampions = magicDamageDealtToChampions;
    }

    public void setPhysicalDamageDealtToChampions(float physicalDamageDealtToChampions) {
        this.physicalDamageDealtToChampions = physicalDamageDealtToChampions;
    }

    public void setTrueDamageDealtToChampions(float trueDamageDealtToChampions) {
        this.trueDamageDealtToChampions = trueDamageDealtToChampions;
    }

    public void setTotalHeal(float totalHeal) {
        this.totalHeal = totalHeal;
    }

    public void setTotalUnitsHealed(float totalUnitsHealed) {
        this.totalUnitsHealed = totalUnitsHealed;
    }

    public void setDamageSelfMitigated(float damageSelfMitigated) {
        this.damageSelfMitigated = damageSelfMitigated;
    }

    public void setDamageDealtToObjectives(float damageDealtToObjectives) {
        this.damageDealtToObjectives = damageDealtToObjectives;
    }

    public void setDamageDealtToTurrets(float damageDealtToTurrets) {
        this.damageDealtToTurrets = damageDealtToTurrets;
    }

    public void setVisionScore(float visionScore) {
        this.visionScore = visionScore;
    }

    public void setTimeCCingOthers(float timeCCingOthers) {
        this.timeCCingOthers = timeCCingOthers;
    }

    public void setTotalDamageTaken(float totalDamageTaken) {
        this.totalDamageTaken = totalDamageTaken;
    }

    public void setMagicalDamageTaken(float magicalDamageTaken) {
        this.magicalDamageTaken = magicalDamageTaken;
    }

    public void setPhysicalDamageTaken(float physicalDamageTaken) {
        this.physicalDamageTaken = physicalDamageTaken;
    }

    public void setTrueDamageTaken(float trueDamageTaken) {
        this.trueDamageTaken = trueDamageTaken;
    }

    public void setGoldEarned(float goldEarned) {
        this.goldEarned = goldEarned;
    }

    public void setGoldSpent(float goldSpent) {
        this.goldSpent = goldSpent;
    }

    public void setTurretKills(float turretKills) {
        this.turretKills = turretKills;
    }

    public void setInhibitorKills(float inhibitorKills) {
        this.inhibitorKills = inhibitorKills;
    }

    public void setTotalMinionsKilled(float totalMinionsKilled) {
        this.totalMinionsKilled = totalMinionsKilled;
    }

    public void setNeutralMinionsKilled(float neutralMinionsKilled) {
        this.neutralMinionsKilled = neutralMinionsKilled;
    }

    public void setNeutralMinionsKilledTeamJungle(float neutralMinionsKilledTeamJungle) {
        this.neutralMinionsKilledTeamJungle = neutralMinionsKilledTeamJungle;
    }

    public void setNeutralMinionsKilledEnemyJungle(float neutralMinionsKilledEnemyJungle) {
        this.neutralMinionsKilledEnemyJungle = neutralMinionsKilledEnemyJungle;
    }

    public void setTotalTimeCrowdControlDealt(float totalTimeCrowdControlDealt) {
        this.totalTimeCrowdControlDealt = totalTimeCrowdControlDealt;
    }

    public void setChampLevel(float champLevel) {
        this.champLevel = champLevel;
    }

    public void setVisionWardsBoughtInGame(float visionWardsBoughtInGame) {
        this.visionWardsBoughtInGame = visionWardsBoughtInGame;
    }

    public void setSightWardsBoughtInGame(float sightWardsBoughtInGame) {
        this.sightWardsBoughtInGame = sightWardsBoughtInGame;
    }

    public void setWardsPlaced(float wardsPlaced) {
        this.wardsPlaced = wardsPlaced;
    }

    public void setWardsKilled(float wardsKilled) {
        this.wardsKilled = wardsKilled;
    }

    public void setFirstBloodKill(boolean firstBloodKill) {
        this.firstBloodKill = firstBloodKill;
    }

    public void setFirstBloodAssist(boolean firstBloodAssist) {
        this.firstBloodAssist = firstBloodAssist;
    }

    public void setFirstTowerKill(boolean firstTowerKill) {
        this.firstTowerKill = firstTowerKill;
    }

    public void setFirstTowerAssist(boolean firstTowerAssist) {
        this.firstTowerAssist = firstTowerAssist;
    }

    public void setFirstInhibitorKill(boolean firstInhibitorKill) {
        this.firstInhibitorKill = firstInhibitorKill;
    }

    public void setFirstInhibitorAssist(boolean firstInhibitorAssist) {
        this.firstInhibitorAssist = firstInhibitorAssist;
    }

    public void setCombatPlayerScore(float combatPlayerScore) {
        this.combatPlayerScore = combatPlayerScore;
    }

    public void setObjectivePlayerScore(float objectivePlayerScore) {
        this.objectivePlayerScore = objectivePlayerScore;
    }

    public void setTotalPlayerScore(float totalPlayerScore) {
        this.totalPlayerScore = totalPlayerScore;
    }

    public void setTotalScoreRank(float totalScoreRank) {
        this.totalScoreRank = totalScoreRank;
    }

    public void setPlayerScore0(float playerScore0) {
        this.playerScore0 = playerScore0;
    }

    public void setPlayerScore1(float playerScore1) {
        this.playerScore1 = playerScore1;
    }

    public void setPlayerScore2(float playerScore2) {
        this.playerScore2 = playerScore2;
    }

    public void setPlayerScore3(float playerScore3) {
        this.playerScore3 = playerScore3;
    }

    public void setPlayerScore4(float playerScore4) {
        this.playerScore4 = playerScore4;
    }

    public void setPlayerScore5(float playerScore5) {
        this.playerScore5 = playerScore5;
    }

    public void setPlayerScore6(float playerScore6) {
        this.playerScore6 = playerScore6;
    }

    public void setPlayerScore7(float playerScore7) {
        this.playerScore7 = playerScore7;
    }

    public void setPlayerScore8(float playerScore8) {
        this.playerScore8 = playerScore8;
    }

    public void setPlayerScore9(float playerScore9) {
        this.playerScore9 = playerScore9;
    }

    public void setPerk0(float perk0) {
        this.perk0 = perk0;
    }

    public void setPerk0Var1(float perk0Var1) {
        this.perk0Var1 = perk0Var1;
    }

    public void setPerk0Var2(float perk0Var2) {
        this.perk0Var2 = perk0Var2;
    }

    public void setPerk0Var3(float perk0Var3) {
        this.perk0Var3 = perk0Var3;
    }

    public void setPerk1(float perk1) {
        this.perk1 = perk1;
    }

    public void setPerk1Var1(float perk1Var1) {
        this.perk1Var1 = perk1Var1;
    }

    public void setPerk1Var2(float perk1Var2) {
        this.perk1Var2 = perk1Var2;
    }

    public void setPerk1Var3(float perk1Var3) {
        this.perk1Var3 = perk1Var3;
    }

    public void setPerk2(float perk2) {
        this.perk2 = perk2;
    }

    public void setPerk2Var1(float perk2Var1) {
        this.perk2Var1 = perk2Var1;
    }

    public void setPerk2Var2(float perk2Var2) {
        this.perk2Var2 = perk2Var2;
    }

    public void setPerk2Var3(float perk2Var3) {
        this.perk2Var3 = perk2Var3;
    }

    public void setPerk3(float perk3) {
        this.perk3 = perk3;
    }

    public void setPerk3Var1(float perk3Var1) {
        this.perk3Var1 = perk3Var1;
    }

    public void setPerk3Var2(float perk3Var2) {
        this.perk3Var2 = perk3Var2;
    }

    public void setPerk3Var3(float perk3Var3) {
        this.perk3Var3 = perk3Var3;
    }

    public void setPerk4(float perk4) {
        this.perk4 = perk4;
    }

    public void setPerk4Var1(float perk4Var1) {
        this.perk4Var1 = perk4Var1;
    }

    public void setPerk4Var2(float perk4Var2) {
        this.perk4Var2 = perk4Var2;
    }

    public void setPerk4Var3(float perk4Var3) {
        this.perk4Var3 = perk4Var3;
    }

    public void setPerk5(float perk5) {
        this.perk5 = perk5;
    }

    public void setPerk5Var1(float perk5Var1) {
        this.perk5Var1 = perk5Var1;
    }

    public void setPerk5Var2(float perk5Var2) {
        this.perk5Var2 = perk5Var2;
    }

    public void setPerk5Var3(float perk5Var3) {
        this.perk5Var3 = perk5Var3;
    }

    public void setPerkPrimaryStyle(float perkPrimaryStyle) {
        this.perkPrimaryStyle = perkPrimaryStyle;
    }

    public void setPerkSubStyle(float perkSubStyle) {
        this.perkSubStyle = perkSubStyle;
    }

    public void setStatPerk0(float statPerk0) {
        this.statPerk0 = statPerk0;
    }

    public void setStatPerk1(float statPerk1) {
        this.statPerk1 = statPerk1;
    }

    public void setStatPerk2(float statPerk2) {
        this.statPerk2 = statPerk2;
    }
}

