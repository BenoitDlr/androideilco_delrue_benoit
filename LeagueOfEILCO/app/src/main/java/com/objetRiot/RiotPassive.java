package com.objetRiot;

public class RiotPassive {
    private String name;
    private String description;
    RiotImage image;


    // Getter Methods

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public RiotImage getImage() {
        return image;
    }

    // Setter Methods

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(RiotImage imageObject) {
        this.image = imageObject;
    }
}
