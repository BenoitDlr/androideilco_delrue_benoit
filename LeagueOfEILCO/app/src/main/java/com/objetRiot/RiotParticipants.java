package com.objetRiot;

public class RiotParticipants {
    private float participantId;
    private float championId;
    private float spell1Id;
    private float spell2Id;
    RiotStat stats;
    private float teamId;

    public float getParticipantId() {
        return participantId;
    }

    public void setParticipantId(float participantId) {
        this.participantId = participantId;
    }

    public float getChampionId() {
        return championId;
    }

    public void setChampionId(float championId) {
        this.championId = championId;
    }

    public float getSpell1Id() {
        return spell1Id;
    }

    public void setSpell1Id(float spell1Id) {
        this.spell1Id = spell1Id;
    }

    public float getSpell2Id() {
        return spell2Id;
    }

    public void setSpell2Id(float spell2Id) {
        this.spell2Id = spell2Id;
    }

    public RiotStat getStatsObject() {
        return stats;
    }

    public void setStatsObject(RiotStat statsObject) {
        stats = statsObject;
    }

    public float getTeamId() {
        return teamId;
    }

    public void setTeamId(float teamId) {
        this.teamId = teamId;
    }
}
