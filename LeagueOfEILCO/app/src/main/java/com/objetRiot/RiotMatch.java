package com.objetRiot;

import java.util.ArrayList;

public class RiotMatch {
    private float gameId;
    private String platformId;
    private float gameCreation;
    private float gameDuration;
    private float queueId;
    private float mapId;
    private float seasonId;
    private String gameVersion;
    private String gameMode;
    private String gameType;
    ArrayList<RiotTeam> teams = new ArrayList < RiotTeam > ();
    ArrayList<RiotParticipants> participants = new ArrayList<RiotParticipants> ();
    ArrayList<RiotParticipantsId> participantIdentities = new ArrayList<RiotParticipantsId> ();

    public float getGameId() {
        return gameId;
    }

    public void setGameId(float gameId) {
        this.gameId = gameId;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public float getGameCreation() {
        return gameCreation;
    }

    public void setGameCreation(float gameCreation) {
        this.gameCreation = gameCreation;
    }

    public float getGameDuration() {
        return gameDuration;
    }

    public void setGameDuration(float gameDuration) {
        this.gameDuration = gameDuration;
    }

    public float getQueueId() {
        return queueId;
    }

    public void setQueueId(float queueId) {
        this.queueId = queueId;
    }

    public float getMapId() {
        return mapId;
    }

    public void setMapId(float mapId) {
        this.mapId = mapId;
    }

    public float getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(float seasonId) {
        this.seasonId = seasonId;
    }

    public String getGameVersion() {
        return gameVersion;
    }

    public void setGameVersion(String gameVersion) {
        this.gameVersion = gameVersion;
    }

    public String getGameMode() {
        return gameMode;
    }

    public void setGameMode(String gameMode) {
        this.gameMode = gameMode;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public ArrayList<RiotTeam> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<RiotTeam> teams) {
        this.teams = teams;
    }

    public ArrayList<RiotParticipants> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<RiotParticipants> participants) {
        this.participants = participants;
    }

    public ArrayList<RiotParticipantsId> getParticipantIdentities() {
        return participantIdentities;
    }

    public void setParticipantIdentities(ArrayList<RiotParticipantsId> participantIdentities) {
        this.participantIdentities = participantIdentities;
    }
}
