package com.objetRiot;

import java.util.ArrayList;

public class RiotTeam {
    private float teamId;
    private String win;
    private boolean firstBlood;
    private boolean firstTower;
    private boolean firstInhibitor;
    private boolean firstBaron;
    private boolean firstDragon;
    private boolean firstRiftHerald;
    private float towerKills;
    private float inhibitorKills;
    private float baronKills;
    private float dragonKills;
    private float vilemawKills;
    private float riftHeraldKills;
    private float dominionVictoryScore;
    ArrayList < RiotTeamBan > bans = new ArrayList< RiotTeamBan >();


    // Getter Methods

    public float getTeamId() {
        return teamId;
    }

    public String getWin() {
        return win;
    }

    public boolean getFirstBlood() {
        return firstBlood;
    }

    public boolean getFirstTower() {
        return firstTower;
    }

    public boolean getFirstInhibitor() {
        return firstInhibitor;
    }

    public boolean getFirstBaron() {
        return firstBaron;
    }

    public boolean getFirstDragon() {
        return firstDragon;
    }

    public boolean getFirstRiftHerald() {
        return firstRiftHerald;
    }

    public float getTowerKills() {
        return towerKills;
    }

    public float getInhibitorKills() {
        return inhibitorKills;
    }

    public float getBaronKills() {
        return baronKills;
    }

    public float getDragonKills() {
        return dragonKills;
    }

    public float getVilemawKills() {
        return vilemawKills;
    }

    public float getRiftHeraldKills() {
        return riftHeraldKills;
    }

    public float getDominionVictoryScore() {
        return dominionVictoryScore;
    }

    // Setter Methods

    public void setTeamId(float teamId) {
        this.teamId = teamId;
    }

    public void setWin(String win) {
        this.win = win;
    }

    public void setFirstBlood(boolean firstBlood) {
        this.firstBlood = firstBlood;
    }

    public void setFirstTower(boolean firstTower) {
        this.firstTower = firstTower;
    }

    public void setFirstInhibitor(boolean firstInhibitor) {
        this.firstInhibitor = firstInhibitor;
    }

    public void setFirstBaron(boolean firstBaron) {
        this.firstBaron = firstBaron;
    }

    public void setFirstDragon(boolean firstDragon) {
        this.firstDragon = firstDragon;
    }

    public void setFirstRiftHerald(boolean firstRiftHerald) {
        this.firstRiftHerald = firstRiftHerald;
    }

    public void setTowerKills(float towerKills) {
        this.towerKills = towerKills;
    }

    public void setInhibitorKills(float inhibitorKills) {
        this.inhibitorKills = inhibitorKills;
    }

    public void setBaronKills(float baronKills) {
        this.baronKills = baronKills;
    }

    public void setDragonKills(float dragonKills) {
        this.dragonKills = dragonKills;
    }

    public void setVilemawKills(float vilemawKills) {
        this.vilemawKills = vilemawKills;
    }

    public void setRiftHeraldKills(float riftHeraldKills) {
        this.riftHeraldKills = riftHeraldKills;
    }

    public void setDominionVictoryScore(float dominionVictoryScore) {
        this.dominionVictoryScore = dominionVictoryScore;
    }
}
