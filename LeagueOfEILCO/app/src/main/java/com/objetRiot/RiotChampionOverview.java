package com.objetRiot;

public class RiotChampionOverview {
    private String type;
    private String format;
    private String version;
    RiotChampData data;


    // Getter Methods

    public String getType() {
        return type;
    }

    public String getFormat() {
        return format;
    }

    public String getVersion() {
        return version;
    }

    public RiotChampData getData() {
        return data;
    }

    // Setter Methods

    public void setType(String type) {
        this.type = type;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setData(RiotChampData dataObject) {
        this.data = dataObject;
    }
}
