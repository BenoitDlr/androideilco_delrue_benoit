package com.objetRiot;

public class RiotChampStat {
    private float hp;
    private float hpperlevel;
    private float mp;
    private float mpperlevel;
    private float movespeed;
    private float armor;
    private float armorperlevel;
    private float spellblock;
    private float spellblockperlevel;
    private float attackrange;
    private float hpregen;
    private float hpregenperlevel;
    private float mpregen;
    private float mpregenperlevel;
    private float crit;
    private float critperlevel;
    private float attackdamage;
    private float attackdamageperlevel;
    private float attackspeedperlevel;
    private float attackspeed;


    // Getter Methods

    public float getHp() {
        return hp;
    }

    public float getHpperlevel() {
        return hpperlevel;
    }

    public float getMp() {
        return mp;
    }

    public float getMpperlevel() {
        return mpperlevel;
    }

    public float getMovespeed() {
        return movespeed;
    }

    public float getArmor() {
        return armor;
    }

    public float getArmorperlevel() {
        return armorperlevel;
    }

    public float getSpellblock() {
        return spellblock;
    }

    public float getSpellblockperlevel() {
        return spellblockperlevel;
    }

    public float getAttackrange() {
        return attackrange;
    }

    public float getHpregen() {
        return hpregen;
    }

    public float getHpregenperlevel() {
        return hpregenperlevel;
    }

    public float getMpregen() {
        return mpregen;
    }

    public float getMpregenperlevel() {
        return mpregenperlevel;
    }

    public float getCrit() {
        return crit;
    }

    public float getCritperlevel() {
        return critperlevel;
    }

    public float getAttackdamage() {
        return attackdamage;
    }

    public float getAttackdamageperlevel() {
        return attackdamageperlevel;
    }

    public float getAttackspeedperlevel() {
        return attackspeedperlevel;
    }

    public float getAttackspeed() {
        return attackspeed;
    }

    // Setter Methods

    public void setHp(float hp) {
        this.hp = hp;
    }

    public void setHpperlevel(float hpperlevel) {
        this.hpperlevel = hpperlevel;
    }

    public void setMp(float mp) {
        this.mp = mp;
    }

    public void setMpperlevel(float mpperlevel) {
        this.mpperlevel = mpperlevel;
    }

    public void setMovespeed(float movespeed) {
        this.movespeed = movespeed;
    }

    public void setArmor(float armor) {
        this.armor = armor;
    }

    public void setArmorperlevel(float armorperlevel) {
        this.armorperlevel = armorperlevel;
    }

    public void setSpellblock(float spellblock) {
        this.spellblock = spellblock;
    }

    public void setSpellblockperlevel(float spellblockperlevel) {
        this.spellblockperlevel = spellblockperlevel;
    }

    public void setAttackrange(float attackrange) {
        this.attackrange = attackrange;
    }

    public void setHpregen(float hpregen) {
        this.hpregen = hpregen;
    }

    public void setHpregenperlevel(float hpregenperlevel) {
        this.hpregenperlevel = hpregenperlevel;
    }

    public void setMpregen(float mpregen) {
        this.mpregen = mpregen;
    }

    public void setMpregenperlevel(float mpregenperlevel) {
        this.mpregenperlevel = mpregenperlevel;
    }

    public void setCrit(float crit) {
        this.crit = crit;
    }

    public void setCritperlevel(float critperlevel) {
        this.critperlevel = critperlevel;
    }

    public void setAttackdamage(float attackdamage) {
        this.attackdamage = attackdamage;
    }

    public void setAttackdamageperlevel(float attackdamageperlevel) {
        this.attackdamageperlevel = attackdamageperlevel;
    }

    public void setAttackspeedperlevel(float attackspeedperlevel) {
        this.attackspeedperlevel = attackspeedperlevel;
    }

    public void setAttackspeed(float attackspeed) {
        this.attackspeed = attackspeed;
    }
}
