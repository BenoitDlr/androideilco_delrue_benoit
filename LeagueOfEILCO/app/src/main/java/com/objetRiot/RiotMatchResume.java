package com.objetRiot;

public class RiotMatchResume {

    private String platformId;
    private long gameId;
    private float champion;
    private float queue;
    private float season;
    private float timestamp;
    private String role;
    private String lane;


    // Getter Methods

    public String getPlatformId() {
        return platformId;
    }

    public long getGameId() {
        return gameId;
    }

    public float getChampion() {
        return champion;
    }

    public float getQueue() {
        return queue;
    }

    public float getSeason() {
        return season;
    }

    public float getTimestamp() {
        return timestamp;
    }

    public String getRole() {
        return role;
    }

    public String getLane() {
        return lane;
    }

    // Setter Methods

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public void setChampion(float champion) {
        this.champion = champion;
    }

    public void setQueue(float queue) {
        this.queue = queue;
    }

    public void setSeason(float season) {
        this.season = season;
    }

    public void setTimestamp(float timestamp) {
        this.timestamp = timestamp;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setLane(String lane) {
        this.lane = lane;
    }
}
