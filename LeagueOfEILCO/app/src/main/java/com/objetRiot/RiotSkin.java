package com.objetRiot;

public class RiotSkin {
    private String id;
    private float num;
    private String name;
    private boolean chromas;


    // Getter Methods

    public String getId() {
        return id;
    }

    public float getNum() {
        return num;
    }

    public String getName() {
        return name;
    }

    public boolean getChromas() {
        return chromas;
    }

    // Setter Methods

    public void setId(String id) {
        this.id = id;
    }

    public void setNum(float num) {
        this.num = num;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChromas(boolean chromas) {
        this.chromas = chromas;
    }
}
