package com.objetRiot;

public class RiotTeamBan {
    private float championId;
    private float pickTurn;

    // Getter Methods

    public float getChampionId() {
        return championId;
    }

    public float getPickTurn() {
        return pickTurn;
    }

    // Setter Methods

    public void setChampionId(float championId) {
        this.championId = championId;
    }

    public void setPickTurn(float pickTurn) {
        this.pickTurn = pickTurn;
    }
}