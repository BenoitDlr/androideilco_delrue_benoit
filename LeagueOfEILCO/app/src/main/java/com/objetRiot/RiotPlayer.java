package com.objetRiot;

public class RiotPlayer {

    private String platformId;
    private String accountId;
    private String summonerName;
    private String summonerId;
    private String currentPlatformId;
    private String currentAccountId;
    private String matchHistoryUri;
    private float profileIcon;


    // Getter Methods

    public String getPlatformId() {
        return platformId;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public String getSummonerId() {
        return summonerId;
    }

    public String getCurrentPlatformId() {
        return currentPlatformId;
    }

    public String getCurrentAccountId() {
        return currentAccountId;
    }

    public String getMatchHistoryUri() {
        return matchHistoryUri;
    }

    public float getProfileIcon() {
        return profileIcon;
    }

    // Setter Methods

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setSummonerName(String summonerName) {
        this.summonerName = summonerName;
    }

    public void setSummonerId(String summonerId) {
        this.summonerId = summonerId;
    }

    public void setCurrentPlatformId(String currentPlatformId) {
        this.currentPlatformId = currentPlatformId;
    }

    public void setCurrentAccountId(String currentAccountId) {
        this.currentAccountId = currentAccountId;
    }

    public void setMatchHistoryUri(String matchHistoryUri) {
        this.matchHistoryUri = matchHistoryUri;
    }

    public void setProfileIcon(float profileIcon) {
        this.profileIcon = profileIcon;
    }
}

