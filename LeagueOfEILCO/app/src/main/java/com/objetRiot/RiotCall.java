package com.objetRiot;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RiotCall {

    @Headers({
            "Accept-Charset: application/x-www-form-urlencoded; charset=UTF-8",
            "X-Riot-Token: RGAPI-33825d59-83a5-4569-8789-982c268ec0a8",
            "Accept-Language: en-US,en;q=0.9",
            "User-Agent:  Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36"
            })
    @GET("/lol/platform/v3/champion-rotations")
    Call<RiotRotation> getFreeRotation();

    @Headers({
            "Accept-Charset: application/x-www-form-urlencoded; charset=UTF-8",
            "X-Riot-Token: RGAPI-33825d59-83a5-4569-8789-982c268ec0a8",
            "Accept-Language: en-US,en;q=0.9",
            "User-Agent:  Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36"
    })
    @GET("/lol/summoner/v4/summoners/by-name/{summonerName}")
    Call<RiotSummoner> getSummonerbyName(@Path("summonerName") String str);

    @Headers({
            "Accept-Charset: application/x-www-form-urlencoded; charset=UTF-8",
            "X-Riot-Token: RGAPI-33825d59-83a5-4569-8789-982c268ec0a8",
            "Accept-Language: en-US,en;q=0.9",
            "User-Agent:  Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36"
    })
    @GET("/lol/match/v4/matchlists/by-account/{encryptedAccountId}?endIndex=10&beginIndex=0")
    Call<RiotMatchHistory> getMatchHistory(@Path("encryptedAccountId") String sumAccountId);

    @Headers({
            "Accept-Charset: application/x-www-form-urlencoded; charset=UTF-8",
            "X-Riot-Token: RGAPI-33825d59-83a5-4569-8789-982c268ec0a8",
            "Accept-Language: en-US,en;q=0.9",
            "User-Agent:  Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36"
    })
    @GET("/lol/match/v4/matches/{matchId}")
    Call<RiotMatch> getMatch(@Path("matchId") long matchId);


    @GET("https://ddragon.leagueoflegends.com/cdn/10.3.1/data/fr_FR/champion/{champName}.json")
    Call<RiotChampionOverview> getChampion(@Path("champName") String champName);
}