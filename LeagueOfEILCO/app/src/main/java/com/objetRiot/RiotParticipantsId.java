package com.objetRiot;

public class RiotParticipantsId {
    private float participantId;
    RiotPlayer player;


    // Getter Methods

    public float getParticipantId() {
        return participantId;
    }

    public RiotPlayer getPlayer() {
        return player;
    }

    // Setter Methods

    public void setParticipantId(float participantId) {
        this.participantId = participantId;
    }

    public void setPlayer(RiotPlayer playerObject) {
        this.player = playerObject;
    }
}
