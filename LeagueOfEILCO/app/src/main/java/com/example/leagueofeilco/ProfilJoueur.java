package com.example.leagueofeilco;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.objetRiot.CHAMPS_IDS;
import com.objetRiot.RiotCall;
import com.objetRiot.RiotMatch;
import com.objetRiot.RiotMatchHistory;
import com.objetRiot.RiotMatchResume;
import com.objetRiot.RiotParticipants;
import com.objetRiot.RiotParticipantsId;
import com.objetRiot.RiotSummoner;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilJoueur extends AppCompatActivity {


    TextView niveauJoueur;
    TextView nomJoueur;
    ImageView imageProfil;
    RecyclerView recyclerViewMatch;
    String lienImageProfilPic = "https://ddragon.leagueoflegends.com/cdn/10.1.1/img/profileicon/";
    int imageInvocateur;
    String nomInvocateur;
    ArrayList<Match> listMatchAffichage = new ArrayList<Match>();

    void ajouterMatch(Match match) {
        this.listMatchAffichage.add(match);
    }

    void refreshrecycler() {
        recyclerViewMatch.setAdapter(new MatchAdaptater(this.listMatchAffichage));
    }

    void refreshUserIcon(int id) {
        Picasso.get().load(lienImageProfilPic + id + ".png").into(imageProfil);
    }


    void showErrorPage() {
        Intent intent = new Intent(this, ErrorPage.class);
        intent.putExtra("invocateur", nomInvocateur);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_joueur);

        final ApplicationContext app = (ApplicationContext) getApplicationContext();


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                nomInvocateur = null;
            } else {
                nomInvocateur = extras.get("invocateur").toString();
            }
        } else {
            nomInvocateur = (String) savedInstanceState.getSerializable("invocateur");
        }


        niveauJoueur = findViewById(R.id.niveauJoueur);
        nomJoueur = findViewById(R.id.nomJoueur);
        imageProfil = findViewById(R.id.imageJoueur);
        nomJoueur.setText(this.nomInvocateur);
        imageProfil.setMinimumHeight((int) (app.getScreenwidth() * 0.35));
        imageProfil.setMinimumWidth((int) (app.getScreenwidth() * 0.35));

        Picasso.get().load(lienImageProfilPic + imageInvocateur + ".png").into(imageProfil);

        recyclerViewMatch = findViewById(R.id.recyclerViewMatch);
        ViewGroup.LayoutParams params = recyclerViewMatch.getLayoutParams();
        params.height = (int) (app.getScreenheight() * 0.70);
        recyclerViewMatch.setLayoutParams(params);


        final RiotCall riotRetroService = RetroFitService.createService(RiotCall.class);
        Call<RiotSummoner> azer = riotRetroService.getSummonerbyName(this.nomInvocateur);
        Toast.makeText(ProfilJoueur.this, "Loading summoner " + this.nomInvocateur + " ...", Toast.LENGTH_SHORT).show();

        azer.enqueue(new Callback<RiotSummoner>() {
            @Override
            public void onResponse(Call<RiotSummoner> call, Response<RiotSummoner> response) {
                final RiotSummoner summonerSearched = response.body();

                if (summonerSearched != null) {
                    Call<RiotMatchHistory> histo = riotRetroService.getMatchHistory(summonerSearched.getAccountId());
                    histo.enqueue(new Callback<RiotMatchHistory>() {
                        @Override
                        public void onResponse(Call<RiotMatchHistory> call, Response<RiotMatchHistory> response) {
                            final RiotMatchHistory matchHistory = response.body();
                            if (matchHistory != null) {
                                ArrayList<RiotMatchResume> listMatch = matchHistory.getMatches();

                                ArrayList<Long> gameIdHisto = new ArrayList<Long>();
                                for (final RiotMatchResume match : listMatch) {

                                    final Call<RiotMatch> callMatch = riotRetroService.getMatch(match.getGameId());
                                    callMatch.enqueue(new Callback<RiotMatch>() {
                                        @Override
                                        public void onResponse(Call<RiotMatch> call, Response<RiotMatch> response) {
                                            RiotMatch currentMatch = response.body();
                                            ArrayList<RiotParticipantsId> allPlayersIdentities = currentMatch.getParticipantIdentities();
                                            ArrayList<RiotParticipants> allPlayers = currentMatch.getParticipants();
                                            //nomInvocateur
                                            float searchedPartId = -1000;
                                            for (RiotParticipantsId partId : allPlayersIdentities) {
                                                String currentName = partId.getPlayer().getSummonerName();
                                                if (currentName.equals(ProfilJoueur.this.nomInvocateur)) {
                                                    searchedPartId = partId.getParticipantId();
                                                    break;
                                                }
                                            }
                                            boolean victoire = false;
                                            String kda = "?/?/?";
                                            float kill = 0;
                                            float death = 0;
                                            float assist = 0;
                                            for (RiotParticipants part : allPlayers) {
                                                if (part.getParticipantId() == searchedPartId) {
                                                    kda = part.getStatsObject().getKDA();
                                                    kill = part.getStatsObject().getKills();
                                                    death = part.getStatsObject().getDeaths();
                                                    assist = part.getStatsObject().getAssists();
                                                    victoire = part.getStatsObject().getWin();
                                                    break;
                                                }
                                            }

                                            //////////////////////////////////////////////////////////////////////////////////////////////
                                            //Info Utilisable et affichable c'est la que tu bosses pierre.
                                            //////////////////////////////////////////////////////////////////////////////////////////////
                                            // durée de la partie
                                            int dureeminute = (int) (currentMatch.getGameDuration() / 60);
                                            int dureeseconde = (int) (currentMatch.getGameDuration() % 60);
                                            String dureesecondestr;
                                            if (dureeseconde < 10) {
                                                dureesecondestr = "0" + dureeseconde;
                                            } else {
                                                dureesecondestr = "" + dureeseconde;
                                            }
                                            String duree = dureeminute + ":" + dureesecondestr;
                                            summonerSearched.getName();
                                            refreshUserIcon(summonerSearched.getProfileIconId());  //Go get l'url de cet ID
                                            niveauJoueur.setText("Niveau : " + summonerSearched.getSummonerLevel());

                                            matchHistory.getTotalGames(); // Nombre de game prise en compte dans la liste.

                                            match.getChampion();
                                            match.getLane();
                                            match.getQueue();
                                            match.getRole();


                                            //kda
                                            //victoire
                                            ajouterMatch(new Match(victoire, CHAMPS_IDS.getName((int) match.getChampion()), (int) kill, (int) death, (int) assist, duree));

                                            refreshrecycler();


                                            //////////////////////////////////////////////////////////////////////////////////////////////
                                            //
                                            //////////////////////////////////////////////////////////////////////////////////////////////
                                        }

                                        @Override
                                        public void onFailure(Call<RiotMatch> call, Throwable t) {
                                            showErrorPage();
                                            Toast.makeText(ProfilJoueur.this, "Unable to load match data ...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<RiotMatchHistory> call, Throwable t) {
                            showErrorPage();
                            Toast.makeText(ProfilJoueur.this, "Unable to load summoner match history ...", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{showErrorPage();}
            }

            @Override
            public void onFailure(Call<RiotSummoner> call, Throwable t) {
                showErrorPage();
                Toast.makeText(ProfilJoueur.this, "Unable to load this summoner ...", Toast.LENGTH_SHORT).show();
            }
        });

        recyclerViewMatch.setLayoutManager(new LinearLayoutManager(this));


    }
}
