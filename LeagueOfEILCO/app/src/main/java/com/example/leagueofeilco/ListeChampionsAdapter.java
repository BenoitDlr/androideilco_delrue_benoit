package com.example.leagueofeilco;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.objetRiot.CHAMPS_IDS;

import java.util.List;

public class ListeChampionsAdapter extends RecyclerView.Adapter<ListeChampionsViewHolder> {

    List<CHAMPS_IDS> listeChampions;
    public ListeChampionsAdapter(List<CHAMPS_IDS> listeChampions) {
        this.listeChampions = listeChampions;
    }

    @NonNull
    @Override
    public ListeChampionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_champion, parent,false);
        return new ListeChampionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListeChampionsViewHolder holder, int position) {
        holder.display(listeChampions.get(position));
    }

    @Override
    public int getItemCount() {
        return listeChampions.size();
    }
}
