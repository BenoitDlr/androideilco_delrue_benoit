package com.example.leagueofeilco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.objetRiot.CHAMPS_IDS;
import com.objetRiot.RiotCall;
import com.objetRiot.RiotChampionOverview;
import com.objetRiot.RiotRotation;
import com.objetRiot.RiotSummoner;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    Button boutonPatch;

    TableLayout layoutRotationChampion;

    EditText barreDeRecherche;

    String urlPatch = "https://euw.leagueoflegends.com/fr-fr/news/game-updates/";
    Intent intentPatch = new Intent(Intent.ACTION_VIEW, Uri.parse(urlPatch));

    String urlBlogDev = "https://euw.leagueoflegends.com/fr-fr/news/dev/";
    Intent intentBlogDev = new Intent(Intent.ACTION_VIEW, Uri.parse(urlBlogDev));

    String urlMedia = "https://euw.leagueoflegends.com/fr-fr/news/media/";
    Intent intentMedia = new Intent(Intent.ACTION_VIEW, Uri.parse(urlMedia));

    String urlHistoire = "https://euw.leagueoflegends.com/fr-fr/news/lore/";
    Intent intentHistoire = new Intent(Intent.ACTION_VIEW, Uri.parse(urlHistoire));

    String urlEsport = "https://euw.leagueoflegends.com/fr-fr/news/esports/";
    Intent intentEsport = new Intent(Intent.ACTION_VIEW, Uri.parse(urlEsport));

    String imagePyke =  "https://ddragon.leagueoflegends.com/cdn/10.1.1/img/champion/Pyke.png";
    String lienImage =  "https://ddragon.leagueoflegends.com/cdn/10.1.1/img/champion/";
    List<Integer> freeChamps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

final ApplicationContext app = (ApplicationContext) getApplicationContext();



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView ImageTest =  findViewById(R.id.imageViewPyke);
        layoutRotationChampion = findViewById(R.id.layoutRotationChampion);
        Picasso.get().load(imagePyke).into(ImageTest);
        barreDeRecherche = findViewById(R.id.nomInvocateur);
        ///////////////////////////////////////////////////////////////
        // Appel API pour les champions gratuits
        ///////////////////////////////////////////////////////////////


        RiotCall riotRetroService = RetroFitService.createService(RiotCall.class);
        Call<RiotRotation> call =  riotRetroService.getFreeRotation();
        Toast.makeText(MainActivity.this, "Loading free champs ...", Toast.LENGTH_SHORT).show();

        call.enqueue(new Callback<RiotRotation>() {
            @Override
            public void onResponse(Call<RiotRotation> call, Response<RiotRotation> response) {
                RiotRotation rotation = response.body();

                if(rotation !=null) {
                    freeChamps = rotation.getFreeChampionIds();

                    //L'endroit ou tu veux convertir id => champion puis utilisé le nom des champions pour recup les images
                    int i=0;
                    TableRow row = new TableRow(getApplicationContext());
                    for (int freeChamp:freeChamps) {

                        ImageView Image = new ImageView(getApplicationContext());
                        Image.setMinimumWidth(app.getScreenwidth()/5);
                        Image.setMinimumHeight(app.getScreenwidth()/5);
                        Picasso.get().load(lienImage+CHAMPS_IDS.getName(freeChamp)+".png").into(Image);
                        row.addView(Image);
                        i++;
                        if(i%5==0&&i!=0){

                            layoutRotationChampion.addView(row);
                            row = new TableRow(getApplicationContext());
                        }

                    }
                    
                }else{
                    Toast.makeText(MainActivity.this,"Error : No rotation found",Toast.LENGTH_SHORT).show();
                }
            }
         @Override
            public void onFailure(Call<RiotRotation> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error : Couldn't load free Rotation", Toast.LENGTH_SHORT).show();
            }
        });

        ///////////////////////////////////////////////////////////////
        //
        ///////////////////////////////////////////////////////////////
    }

    public void onClickBoutonPatch(View v){
        startActivity(intentPatch);
    }

    public void onClickBoutonBlogDev(View v){
        startActivity(intentBlogDev);
    }

    public void onClickBoutonMedia(View v){
        startActivity(intentMedia);
    }

    public void onClickBoutonHistoire(View v){
        startActivity(intentHistoire);
    }

    public void onClickBoutonApropos(View v){
        Intent intent = new Intent(this, Apropos.class);
        startActivity(intent);
    }

    public void onClickBoutonChercherjoueur(View v){

        if( TextUtils.isEmpty(barreDeRecherche.getText())){
            /**
             *   You can Toast a message here that the Username is Empty
             **/

            barreDeRecherche.setError( "Entrez un nom d'invocateur!" );

        }else {
            Intent intent = new Intent(this, ProfilJoueur.class);
            intent.putExtra("invocateur", barreDeRecherche.getText());
            startActivity(intent);
        }
    }

    public void onClickBoutonListChamps(View v) {
        Intent intent = new Intent(this, ListeChampionsActivity.class);
        startActivity(intent);
    }


}
