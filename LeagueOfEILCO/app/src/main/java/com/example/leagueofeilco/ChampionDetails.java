package com.example.leagueofeilco;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.objetRiot.CHAMPS_IDS;
import com.objetRiot.RiotCall;
import com.objetRiot.RiotChampion;
import com.objetRiot.RiotChampionOverview;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChampionDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_champion_details);
        final String lienImageChampion =  "https://ddragon.leagueoflegends.com/cdn/10.1.1/img/champion/";
        final ImageView imageChampion = findViewById(R.id.imageChampion);
        final TextView nomChampion = findViewById(R.id.nomChampion);
        final TextView lore = findViewById(R.id.loreFull);

        final ApplicationContext app = (ApplicationContext) getApplicationContext();

        imageChampion.setMinimumHeight((int)(app.getScreenwidth()*0.4));;
        imageChampion.setMinimumWidth((int)(app.getScreenwidth()*0.4));;

        String champion;
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            champion = null;
        }
        else {
            champion = extras.getString("champion");
        }

        RiotCall riotRetroService = RetroFitService.createService(RiotCall.class);
        Call<RiotChampionOverview> call = riotRetroService.getChampion(champion);
        call.enqueue(new Callback<RiotChampionOverview>() {
            @Override
            public void onResponse(Call<RiotChampionOverview> call, Response<RiotChampionOverview> response) {
                RiotChampionOverview overview = response.body();
                if(overview != null){
                    RiotChampion currentChampion = overview.getData().getChamp();
                    int idChamp = Integer.parseInt(currentChampion.getKey());
                    Picasso.get().load(lienImageChampion + CHAMPS_IDS.getName(idChamp) +".png").into(imageChampion);
                    nomChampion.setText(currentChampion.getName());
                    lore.setText(currentChampion.getLore());
                }
            }

            @Override
            public void onFailure(Call<RiotChampionOverview> call, Throwable t) {
                Toast.makeText(ChampionDetails.this, "Error : No such champion", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
