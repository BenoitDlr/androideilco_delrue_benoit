package com.example.leagueofeilco;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.objetRiot.CHAMPS_IDS;

import java.util.Arrays;
import java.util.List;

public class ListeChampionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_champions);

        List<CHAMPS_IDS> listeChampions = Arrays.asList(CHAMPS_IDS.values());

        RecyclerView recyclerView = findViewById(R.id.recyclerViewChampion);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter( new ListeChampionsAdapter(listeChampions));
    }
}
