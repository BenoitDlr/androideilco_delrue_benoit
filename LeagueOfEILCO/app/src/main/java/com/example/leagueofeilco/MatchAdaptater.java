package com.example.leagueofeilco;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MatchAdaptater extends RecyclerView.Adapter<MatchViewHolder>  {

    private List<Match> matchList;

    public MatchAdaptater(List<Match> matchList) {
        this.matchList = matchList;
    }

    public void addMatch(Match match) {
        this.matchList.add(match) ;
    }

    @NonNull
    @Override
    public MatchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_match,
                parent, false);




        return new MatchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchViewHolder holder, int position) {
        holder.display(matchList.get(position));
    }

    @Override
    public int getItemCount() {
        return matchList.size();
    }
}
