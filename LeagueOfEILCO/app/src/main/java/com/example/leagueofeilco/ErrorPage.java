package com.example.leagueofeilco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ErrorPage extends AppCompatActivity {

    TextView nomJoueur;
    ImageView imageErreur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final ApplicationContext app = (ApplicationContext) getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_page);

        imageErreur=findViewById(R.id.imageErreur);


        imageErreur.setMaxHeight((int)(app.getScreenheight()*0.5));

        String nomInvocateur;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                nomInvocateur= null;
            } else {
                nomInvocateur= extras.get("invocateur").toString();
            }
        } else {
            nomInvocateur= (String) savedInstanceState.getSerializable("invocateur");
        }

        nomJoueur=findViewById(R.id.nomJoueur);
        nomJoueur.setText(nomInvocateur);
    }


    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
