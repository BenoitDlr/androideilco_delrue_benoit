package com.example.leagueofeilco;

import android.graphics.Color;

public class Match {

    public Match(String etat, String champion, int kill, int death, int assist) {
        this.etat = etat;
        this.champion = champion;
        this.kill = kill;
        this.death = death;
        this.assist = assist;
        if(etat.equals("remake")){this.textColor="#2796bc";}
        if(etat.equals("win") ){this.textColor="#3CBC8D";}
        if(etat.equals("lose")){this.textColor="#e9422e";}

    }

    public Match(Boolean boolVictoire, String champion, int kill, int death, int assist, String duree) {
        if(boolVictoire){this.etat="Victoire";}else{this.etat="Défaite";}

        this.champion = champion;
        this.kill = kill;
        this.death = death;
        this.assist = assist;
        this.duree=duree;
        if(etat.equals("remake")){this.textColor="#2796bc";}
        if(etat.equals("Victoire") ){this.textColor="#3CBC8D";}
        if(etat.equals("Défaite")){this.textColor="#e9422e";}

    }

    private String etat;
    private String champion;
    private String textColor;
    private int kill;
    private int death;
    private int assist;
    private String duree;

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getEtat() {
        return etat;
    }

    public String getChampion() {
        return champion;
    }

    public String getTextColor() {
        return textColor;
    }

    public int getKill() {
        return kill;
    }

    public int getDeath() {
        return death;
    }

    public int getAssist() {
        return assist;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public void setChampion(String champion) {
        this.champion = champion;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public void setKill(int kill) {
        this.kill = kill;
    }

    public void setDeath(int death) {
        this.death = death;
    }

    public void setAssist(int assist) {
        this.assist = assist;
    }
}
