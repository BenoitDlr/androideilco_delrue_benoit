package com.example.leagueofeilco;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.objetRiot.CHAMPS_IDS;
import com.squareup.picasso.Picasso;

public class ListeChampionsViewHolder extends RecyclerView.ViewHolder {
    private TextView nomChampion;
    private ImageView imageChampion;
    String lienImageChampion =  "https://ddragon.leagueoflegends.com/cdn/10.1.1/img/champion/";

    public ListeChampionsViewHolder(@NonNull final View itemView) {
        super(itemView);

        nomChampion = itemView.findViewById(R.id.nomChampion);
        imageChampion = itemView.findViewById(R.id.imageChampion);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, ChampionDetails.class);
                intent.putExtra("champion", nomChampion.getText());
                context.startActivity(intent);
            }
        });

        ApplicationContext app = (ApplicationContext) imageChampion.getContext().getApplicationContext();

        imageChampion.setMinimumWidth(app.getScreenwidth()/4);
        imageChampion.setMinimumHeight(app.getScreenwidth()/4);
    }

    void display(CHAMPS_IDS champ) {
        nomChampion.setText(champ.name());
        Picasso.get().load(lienImageChampion+ champ.name() +".png").into(imageChampion);
    }
}
