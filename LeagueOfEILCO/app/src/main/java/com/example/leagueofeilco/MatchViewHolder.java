package com.example.leagueofeilco;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.objetRiot.CHAMPS_IDS;
import com.squareup.picasso.Picasso;

public class MatchViewHolder extends RecyclerView.ViewHolder{

    ImageView imageChampion;
    TextView dureePartie;
    TextView etat;
    TextView score;
    String lienImageChampion =  "https://ddragon.leagueoflegends.com/cdn/10.1.1/img/champion/";

    public MatchViewHolder(@NonNull View itemView) {
        super(itemView);
        etat = itemView.findViewById(R.id.textEtat);
        dureePartie= itemView.findViewById(R.id.dureePartie);
                score = itemView.findViewById(R.id.textScore);
        imageChampion = itemView.findViewById(R.id.imageChampion);


        ApplicationContext app = (ApplicationContext) imageChampion.getContext().getApplicationContext();

        imageChampion.setMinimumWidth(app.getScreenwidth()/4);
        imageChampion.setMinimumHeight(app.getScreenwidth()/4);



    }

    void display(Match match){

        this.dureePartie.setText(match.getDuree());
        this.etat.setText(match.getEtat());
        this.etat.setTextColor(Color.parseColor(match.getTextColor()));
        this.score.setText(match.getKill()+" / "+match.getDeath()+" / "+match.getAssist());
        Picasso.get().load(lienImageChampion+ match.getChampion()+".png").into(imageChampion);
    }
}
