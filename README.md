**AndroidEILCO_DELRUE_BENOIT**

Répertoire contenant l'ensemble des dossiers de TD et TP réalisés au cours du module de développement Android ainsi que le projet.

Concernant le projet, il a été réalisé par :
- ANDRIEUX Pierre
- DELRUE Benoit
- SAUVAGE Maximilien

L'application effectue ses appels à l'aide d'une clé personnelle fournie par l'API Riot Games.
En raison de la politique interne à l'API, plus de 20 requêtes par seconde et plus de 100 requêtes toutes les 2 minutes ne sont pas tolérées.
Je vous remercie par avance de prendre en considération ce détail au cours des tests.

L'application en elle-même est un petit applicatif qui permet d'effectuer des recherches par nom de joueur sur le jeu League of Legends.
En plus de cette recherche, l'application offre la possibilité de voir les champions disponibles et leur histoire ainsi que la rotation hebdomadaire.
Enfin, plusieurs liens permettent de renvoyer aux forums de League of Legends pour trouver plus d'informations concernant le jeu