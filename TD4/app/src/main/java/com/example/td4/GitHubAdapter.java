package com.example.td4;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GitHubAdapter  extends RecyclerView.Adapter<GitHubViewHolder> {
    private List<RepoList> repoList;

    public GitHubAdapter(List<RepoList> repoList) {
        this.repoList = repoList;
    }

    @NonNull
    @Override
    public GitHubViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_github,
                parent, false);
        return new GitHubViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GitHubViewHolder holder, int position) {
        holder.display(repoList.get(position));
    }
    @Override
    public int getItemCount() {
        return repoList.size();
    }
}
