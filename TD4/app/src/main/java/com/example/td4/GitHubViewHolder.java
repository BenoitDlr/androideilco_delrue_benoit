package com.example.td4;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GitHubViewHolder extends RecyclerView.ViewHolder {
    private TextView nameRepo;

    public GitHubViewHolder(@NonNull View itemView) {
        super(itemView);
        nameRepo = itemView.findViewById(R.id.nameRepo);
    }

    void display(RepoList repoList) {
        nameRepo.setText(repoList.getRepoName());
    }
}
