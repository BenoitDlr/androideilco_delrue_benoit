package com.example.td2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewsList";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Button okBtn = findViewById(R.id.okBtn);
        okBtn.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        NewsListApplication app = (NewsListApplication) getApplicationContext();
        String login = app.getLogin();
        if(v.getId() == R.id.okBtn) {
            intent = new Intent(this, NewsActivity.class);
            intent.putExtra("login", login);
            startActivity(intent);
            this.finish();
        }
    }

    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "Terminaison de l'activité : " + getLocalClassName());
    }
}
