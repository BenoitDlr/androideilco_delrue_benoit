package com.example.td2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewsList";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "Lancement de l'activité : " + getLocalClassName());
        setContentView(R.layout.activity_login);
        Button btn = findViewById(R.id.logButton);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        EditText login = findViewById(R.id.login);
        String loginTxt = login.getText().toString();
        NewsListApplication app = (NewsListApplication) getApplicationContext();
        app.setLogin(loginTxt);
        if(v.getId() == R.id.logButton) {
            intent = new Intent(this, NewsActivity.class);
            intent.putExtra("login", loginTxt);
            startActivity(intent);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "Terminaison de l'activité : " + getLocalClassName());
    }
}
