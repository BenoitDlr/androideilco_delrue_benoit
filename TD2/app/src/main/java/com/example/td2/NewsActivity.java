package com.example.td2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewsList";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "Lancement de l'activité : " + getLocalClassName());
        setContentView(R.layout.activity_news);
        Button logoutBtn = findViewById(R.id.logoutBtn);
        Button aboutBtn = findViewById(R.id.aboutBtn);
        Button detailsBtn = findViewById(R.id.detailsBtn);
        logoutBtn.setOnClickListener(this);
        aboutBtn.setOnClickListener(this);
        detailsBtn.setOnClickListener(this);

        Intent intentOld = getIntent();
        String login = intentOld.getStringExtra("login");
        TextView logName = findViewById(R.id.logDisplay);
        logName.setText("Bonjour " + login);
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        if(v.getId() == R.id.logoutBtn) {
            intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            this.finish();
        }
        else if(v.getId() == R.id.aboutBtn) {
            String url = "http://android.busin.fr/";
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }
        else if(v.getId() == R.id.detailsBtn) {
            intent = new Intent(this, DetailsActivity.class);
            startActivity(intent);
            this.finish();
        }
    }

    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "Terminaison de l'activité : " + getLocalClassName());
    }
}
