package com.example.td3;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //CREER ICI UNE LISTE DE JEUX VIDEO NOMMEE mesJeux ET REMPLISSEZ LA DE JeuxVideo
        ArrayList<VideoGame> mesJeux = new ArrayList<VideoGame>();
        mesJeux.add(new VideoGame("God Of War IV", (float)49.99));
        mesJeux.add(new VideoGame("Pokemon Sword", (float)49.99));
        mesJeux.add(new VideoGame("The Witcher III : Wild Hunt", (float)39.99));
        mesJeux.add(new VideoGame("Super Smash Bros. Ultimate", (float)29.99));
        mesJeux.add(new VideoGame("Goat Simulator", (float)9.99));
        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),3));
        myRecyclerView.setAdapter( new MyVideoGamesAdapter(mesJeux));
    }
}
